FROM nginx:latest
RUN mkdir -p /etc/ssl/certs/
RUN mkdir -p /etc/ssl/private/
RUN mkdir -p /usr/share/nginx/html/.well-known/pki-validation

COPY cert/dev/certificate.crt /etc/ssl/certs/nginx.crt
COPY cert/dev/private.key /etc/ssl/private/nginx.key
COPY cert/dev/minio-dev.cer /etc/ssl/certs/ca.crt
COPY cert/dev/E83BD74A94DF03F1A0477E44BDE70FDF.txt /usr/share/nginx/html/.well-known/pki-validation/E83BD74A94DF03F1A0477E44BDE70FDF.txt
COPY cert/dev/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]